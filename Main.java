package myhomework2;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        char[][] matrix = new char[5][5];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = '-';
            }
        }

        Random random = new Random();
        int verticalOrHorizontal = random.nextInt(2);
        int targetLine;
        int targetLine2;
        int targetLine3;
        int targetVertical;
        int targetVertical2;
        int targetVertical3;
        int index; // 0 - горизонталь, 1 - вертикаль
        int horizontalKillings1 = 0;
        int horizontalKillings2 = 0;
        int horizontalKillings3 = 0;
        int verticalKillings1 = 0;
        int verticalKillings2 = 0;
        int verticalKillings3 = 0;
        int killedFlags = 0;

        if (verticalOrHorizontal == 0) {
            targetLine = random.nextInt(3);
            targetVertical = random.nextInt(3);
            index = 0;

        } else {
            targetVertical = random.nextInt(3);
            targetLine = random.nextInt(3);
            index = 1;
        }
        targetLine2 = targetLine + 1;
        targetLine3 = targetLine2 + 1;
        targetVertical2 = targetVertical + 1;
        targetVertical3 = targetVertical2 + 1;
        Scanner scanner = new Scanner(System.in);
        System.out.println("All set. Get ready to rumble!");
        int horizontalPoint;
        int verticalPoint;
        if (index == 0) {
            matrix[targetLine][targetVertical] = '$';
            matrix[targetLine2][targetVertical] = '$';
            matrix[targetLine3][targetVertical] = '$';
            while (horizontalKillings1 + horizontalKillings2 + horizontalKillings3 != 3) {
                System.out.println("Insert the number of line, please!");
                while (!scanner.hasNextInt()) {
                    scanner.nextLine();
                    System.out.println("Wrong insert. Insert the number, please!");
                }
                while ((horizontalPoint = scanner.nextInt()) < 1 | (horizontalPoint > 5)) {
                    System.out.println("Wrong insert. Insert the number from 1 to 5, please!");
                }
                System.out.println("Insert the number of vertival, please!");
                while (!scanner.hasNextInt()) {
                    scanner.nextLine();
                    System.out.println("Wrong insert. Insert the number, please!");
                }
                while ((verticalPoint = scanner.nextInt()) < 1 | (verticalPoint > 5)) {
                    System.out.println("Wrong insert. Insert the number from 1 to 5, please!");
                }
                matrix[--horizontalPoint][--verticalPoint] = '*';
                if ((matrix[targetLine][targetVertical] == matrix[horizontalPoint][verticalPoint])) {
                    killedFlags++;
                    horizontalKillings1 = 1;
                    System.out.println("Good shot");
                    matrix[horizontalPoint][verticalPoint] = 'x';
                    printMatrix(matrix);
                    continue;
                }
                if ((matrix[targetLine2][targetVertical] == matrix[horizontalPoint][verticalPoint])) {
                    killedFlags++;
                    horizontalKillings2 = 1;
                    System.out.println("Good shot");
                    matrix[horizontalPoint][verticalPoint] = 'x';
                    printMatrix(matrix);
                    continue;
                }
                if ((matrix[targetLine3][targetVertical] == matrix[horizontalPoint][verticalPoint])) {
                    killedFlags++;
                    horizontalKillings3 = 1;
                    System.out.println("Good shot");
                    matrix[horizontalPoint][verticalPoint] = 'x';
                    printMatrix(matrix);
                    continue;
                }
                printMatrix(matrix);
            }
            System.out.println("You have won!");
        } else {
            matrix[targetLine][targetVertical] = '$';
            matrix[targetLine][targetVertical2] = '$';
            matrix[targetLine][targetVertical3] = '$';
            while (verticalKillings1 + verticalKillings2 + verticalKillings3 != 3) {
                System.out.println("Insert the number of line, please!");
                while (!scanner.hasNextInt()) {
                    scanner.nextLine();
                    System.out.println("Wrong insert. Insert the number, please!");
                }
                while ((horizontalPoint = scanner.nextInt()) < 1 | (horizontalPoint > 5)) {
                    System.out.println("Wrong insert. Insert the number from 1 to 5, please!");
                }
                System.out.println("Insert the number of vertival, please!");
                while (!scanner.hasNextInt()) {
                    scanner.nextLine();
                    System.out.println("Wrong insert. Insert the number, please!");
                }
                while ((verticalPoint = scanner.nextInt()) < 1 | (verticalPoint > 5)) {
                    System.out.println("Wrong insert. Insert the number from 1 to 5, please!");
                }
                matrix[--horizontalPoint][--verticalPoint] = '*';
                if ((matrix[targetLine][targetVertical] == matrix[horizontalPoint][verticalPoint])) {
                    killedFlags++;
                    verticalKillings1 = 1;
                    System.out.println("Good shot");
                    matrix[horizontalPoint][verticalPoint] = 'x';
                    printMatrix(matrix);
                    continue;
                }
                if ((matrix[targetLine][targetVertical2] == matrix[horizontalPoint][verticalPoint])) {
                    killedFlags++;
                    verticalKillings2 = 1;
                    System.out.println("Good shot");
                    matrix[horizontalPoint][verticalPoint] = 'x';
                    printMatrix(matrix);
                    continue;
                }
                if ((matrix[targetLine][targetVertical3] == matrix[horizontalPoint][verticalPoint])) {
                    killedFlags++;
                    verticalKillings3 = 1;
                    System.out.println("Good shot");
                    matrix[horizontalPoint][verticalPoint] = 'x';
                    printMatrix(matrix);
                    continue;
                }
                printMatrix(matrix);
            }
            System.out.println("You have won!");
        }
    }

    public static void printMatrix(char[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            System.out.print("| ");
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j]);
                System.out.print(" | ");
            }
            System.out.println();
        }
    }
}